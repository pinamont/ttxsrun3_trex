import matplotlib. pyplot as plt
from matplotlib.patches import Patch
from matplotlib.legend_handler import HandlerErrorbar
import numpy as np
import mplhep as hep
hep.style.use(hep.style.ATLAS)
#from ROOT import TLatex,TCanvas

print("Initiating...")
#values
#measurement
data_nom=1.2
data_stat=[0.006, 0.006] #first is down, second is up
data_syst=[np.sqrt(data_stat[0]**2+0.017**2), np.sqrt(data_stat[1]**2+0.017**2)] #uncertainty including stat and syst
data_lumi=[np.sqrt(data_syst[0]**2+0.005**2), np.sqrt(data_syst[1]**2+0.005**2)] #total uncertainty including stat, syst and lumi

fig, ax = plt.subplots()
fig.set_figheight(13)
fig.set_figwidth(10)
#set labels
ax.set_xlabel(r"R$_{t\bar{t}/Z}$", loc="right", va="top")
ax.set_yticks([])
[x.set_linewidth(1.5) for x in ax.spines.values()]
ax.xaxis.set_tick_params(width=1.5)
ax.axvspan(data_nom-data_lumi[0], data_nom+data_lumi[1], color='limegreen')
ax.axvspan(data_nom-data_syst[0], data_nom+data_syst[1], color='#84adef')
ax.axvspan(data_nom-data_stat[0], data_nom+data_stat[1], color='yellow')
ax.axvline(data_nom, color="red")


pa1 = Patch(facecolor='red', edgecolor='red')
pa2 = Patch(facecolor='limegreen', edgecolor='limegreen')
pa3 = Patch(facecolor='#84adef', edgecolor='#84adef')
pa4 = Patch(facecolor='yellow', edgecolor='yellow')

first_legend = ax.legend(handles=[pa2, pa3, pa4, pa2, pa3, pa4, pa2, pa3, pa4, pa1, pa1, pa1, pa2, pa3, pa4, pa2, pa3, pa4, pa2, pa3, pa4],
          labels=['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'data \u00B1 stat. \u00B1 exp. \u00B1 lumi.', 'data \u00B1 stat. \u00B1 exp.', 'data \u00B1 stat. uncertainty'],
          ncol=7, handletextpad=1.0, handlelength=0.3, columnspacing=-1.05,
          loc='upper left', bbox_to_anchor=(0, 0.9), frameon=False, fontsize=14)

trial = plt.gca().add_artist(first_legend)

y_max=np.linspace(1,11,11)
gap=0.25

plt.text(0.92, y_max[10], "Data 2022", verticalalignment='center', horizontalalignment="left", fontsize=16)
#plt.text(1.02, y_max[13]-gap, "11.3 fb$^{-1}$", verticalalignment='center', horizontalalignment="left", fontsize=12)
eb0=plt.errorbar(x=data_nom, y=y_max[10], fmt="o", xerr=np.array([[data_lumi[0],data_lumi[1]]]).T, ecolor="black", capsize=4, color="black", zorder=10, ls='-.')
plt.errorbar(x=data_nom, y=y_max[10], fmt="o", xerr=np.array([[data_syst[0],data_syst[1]]]).T, ecolor="black", capsize=4, color="black", zorder=10, ls='--')
plt.errorbar(x=data_nom, y=y_max[10], fmt="o", xerr=np.array([[data_stat[0],data_stat[1]]]).T, ecolor="black", capsize=4, color="black", zorder=10, ls='-')

ax.hlines(y=y_max[9], xmin=0.91, xmax=1.15, linewidth=1, color='black', linestyle='-.')

plt.text(0.92, y_max[8], r"PDF4LHC21", verticalalignment='center', horizontalalignment="left", fontsize=16)
plt.text(0.92, y_max[8]-gap, r"$m_t=171.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=12)
eb2=plt.errorbar(x=1.2796, y=y_max[8], fmt="^", xerr=np.array([[0.0782,0.0782]]).T, ecolor="black", capsize=4, color="black", zorder=10)
eb2[-1][0].set_linestyle('--')

plt.text(0.92, y_max[7], r"PDF4LHC21", verticalalignment='center', horizontalalignment="left", fontsize=16)
plt.text(0.92, y_max[7]-gap, r"$m_t=172.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=12)
eb3=plt.errorbar(x=1.2453, y=y_max[7], fmt="^", xerr=np.array([[0.0761,0.0761]]).T, ecolor="black", capsize=4, color="black", zorder=10)
eb3[-1][0].set_linestyle('--')

plt.text(0.92, y_max[6], r"PDF4LHC21", verticalalignment='center', horizontalalignment="left", fontsize=16)
plt.text(0.92, y_max[6]-gap,r"$m_t=173.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=12)
eb4=plt.errorbar(x=1.2121, y=y_max[6], fmt="^", xerr=np.array([[0.0741,0.0741]]).T, ecolor="black", capsize=4, color="black", zorder=10)
eb4[-1][0].set_linestyle('--')

plt.text(0.92, y_max[5], r"CT18", verticalalignment='center', horizontalalignment="left", fontsize=16)
plt.text(0.92, y_max[5]-gap, r"$m_t=172.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=12)
eb5=plt.errorbar(x=1.2768, y=y_max[5], fmt="^", xerr=np.array([[0.0204,0.19875]]).T, ecolor="black", capsize=4, color="black", zorder=10)
eb5[-1][0].set_linestyle('--')

plt.text(0.92, y_max[4], r"CT18A", verticalalignment='center', horizontalalignment="left", fontsize=16)
plt.text(0.92, y_max[4]-gap, r"$m_t=172.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=12)
eb6=plt.errorbar(x=1.2529, y=y_max[4], fmt="^", xerr=np.array([[0.0731,0.0402]]).T, ecolor="black", capsize=4, color="black", zorder=10)
eb6[-1][0].set_linestyle('--')

plt.text(0.92, y_max[3], r"MSHT20", verticalalignment='center', horizontalalignment="left", fontsize=16)
plt.text(0.92, y_max[3]-gap, r"$m_t=172.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=12)
eb7=plt.errorbar(x=1.2375, y=y_max[3], fmt="^", xerr=np.array([[0.0254,0.1696]]).T, ecolor="black", capsize=4, color="black", zorder=10)
eb7[-1][0].set_linestyle('--')

plt.text(0.92, y_max[2], r"NNPDF4.0", verticalalignment='center', horizontalalignment="left", fontsize=16)
plt.text(0.92, y_max[2]-gap, r"$m_t=172.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=12)
eb8=plt.errorbar(x=1.218, y=y_max[2], fmt="^", xerr=np.array([[0.014,0.014]]).T, ecolor="black", capsize=4, color="black", zorder=10)
eb8[-1][0].set_linestyle('--')

plt.text(0.92, y_max[1], r"ATLASpdf21", verticalalignment='center', horizontalalignment="left", fontsize=16)
plt.text(0.92, y_max[1]-gap, r"$m_t=172.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=12)
eb9=plt.errorbar(x=1.252, y=y_max[1], fmt="^", xerr=np.array([[0.155,0.094]]).T, ecolor="black", capsize=4, color="black", zorder=10)
eb9[-1][0].set_linestyle('--')

plt.text(0.92, y_max[0], r"ABMP16", verticalalignment='center', horizontalalignment="left", fontsize=16)
plt.text(0.92, y_max[0]-gap, r"$m_t=172.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=12)
eb10=plt.errorbar(x=1.1342, y=y_max[0], fmt="^", xerr=np.array([[0.038,0.038]]).T, ecolor="black", capsize=4, color="black", zorder=10)
eb10[-1][0].set_linestyle('--')

plt.legend(handles=[eb0, eb2],
          handler_map={type(eb2): HandlerErrorbar(xerr_size=3.2)},
          labels=['combined result', 'theory prediction'],
          ncol=1, handletextpad=1.0, handlelength=6,
          loc='upper right', bbox_to_anchor=(1.02, 0.9), frameon=False, fontsize=14)

#ATLAS label
print("Plotting CME label...")
# def ATLAS_LABEL(x,y,tsize=0.05,xofs=0.135,alignv=False):
#         l=TLatex()
#         l.SetNDC()
#         l.SetTextFont(72)
#         l.SetTextColor(1)
#         l.SetTextSize(tsize)
#         l.DrawLatex(x,y,"ATLAS")
#         l2=TLatex()
#         l2.SetNDC()
#         l2.SetTextColor(1)
#         l2.SetTextFont(42)
#         l2.SetTextSize(tsize*0.75)
#         l2.SetTextSize(tsize*1.)
#         if alignv:
#             l2.DrawLatex(x,y-0.05,"Preliminary")
#             #l2.DrawLatex(x,y-0.05,"Internal")
#             pass
#         else:
#             l2.DrawLatex(x+xofs,y,"Preliminary")
#             #l2.DrawLatex(x+xofs,y,"Internal")
#             pass

# #ATLAS label
# canvas=TCanvas("c1","c1",0,0,1000,1300)
# canvas.cd()
# ATLAS_LABEL(0.5,0.5,0.06,0.11)
hep.atlas.label(label="", rlabel=r"$\sqrt{s}=13.6\,$TeV,$\;11.3\,\,\mathrm{fb^{-1}}$", data=True, loc=4, fontsize=20)
plt.text(1.03, 14.0, r"Preliminary", verticalalignment='center', horizontalalignment="left", fontsize=22)

#adapt this to have nice plot aspect ratio
plt.xlim(0.9, 1.5)
plt.ylim(0, 15)
fig.savefig("Xsec_ratio_vs_Theory.pdf")
fig.savefig("Xsec_ratio_vs_Theory.png")
