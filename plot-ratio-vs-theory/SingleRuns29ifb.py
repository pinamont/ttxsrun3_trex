import matplotlib.pyplot as plt
from matplotlib.patches import Patch, Rectangle
from matplotlib.legend_handler import HandlerErrorbar
import numpy as np
import mplhep as hep
hep.style.use(hep.style.ATLAS)

#values
#measurement
data_nom=1.145
data_stat=[0.003, 0.003] #first is down, second is up
data_syst=[0.02131, 0.02121] #uncertainty including stat and syst
data_lumi=[0.02131, 0.02131] #total uncertainty including stat, syst and lumi

fig, ax = plt.subplots()
fig.set_figheight(13)
fig.set_figwidth(13)
#set labels
ax.set_xlabel(r"R$_{t\bar{t}/Z}$", loc="right", va="top",fontsize=30)
ax.set_yticks([])
[x.set_linewidth(1.5) for x in ax.spines.values()]
ax.xaxis.set_tick_params(width=1.5)
#ax.axvspan(data_nom-data_lumi[0], data_nom+data_lumi[1], color='limegreen')
ax.axvspan(data_nom-data_syst[0], data_nom+data_syst[1], color='#84adef')
ax.axvspan(data_nom-data_stat[0], data_nom+data_stat[1], color='yellow')
ax.axvline(data_nom, color="red")


pa1 = Patch(facecolor='red', edgecolor='red')
pa2 = Patch(facecolor='limegreen', edgecolor='limegreen')
pa3 = Patch(facecolor='#84adef', edgecolor='#84adef')
pa4 = Patch(facecolor='yellow', edgecolor='yellow')

first_legend = ax.legend(handles=[pa3, pa4, pa3, pa4, pa3, pa4, pa1, pa1, pa3, pa4, pa3, pa4, pa3, pa4],
          labels=['', '', '', '', '', '', '', '', '', '', '', '', 'data \u00B1 stat. \u00B1 syst. \u00B1 lumi. ', 'data \u00B1 stat. uncertainty'],
          ncol=7, handletextpad=1.0, handlelength=0.3, columnspacing=-1.05,
          loc='upper left', bbox_to_anchor=(0, 0.9), frameon=False, fontsize=18)

trial = plt.gca().add_artist(first_legend)

y_max=np.linspace(1,11,11)
gap=0.30

legend_rect = Patch(facecolor='none', edgecolor='#474747', hatch='//', label='Error Box')

plt.text(0.84, y_max[10], "Data 2022", verticalalignment='center', horizontalalignment="left", fontsize=20)
#plt.text(1.02, y_max[13]-gap, "11.3 fb$^{-1}$", verticalalignment='center', horizontalalignment="left", fontsize=15)
eb0=plt.errorbar(x=data_nom, y=y_max[10], fmt="o", xerr=np.array([[data_lumi[0],data_lumi[1]]]).T, ecolor="black", capsize=5, color="black", zorder=10, ls='-.')
plt.errorbar(x=data_nom, y=y_max[10], fmt="o", xerr=np.array([[data_syst[0],data_syst[1]]]).T, ecolor="black", capsize=5, color="black", zorder=10, ls='--')
plt.errorbar(x=data_nom, y=y_max[10], fmt="o", xerr=np.array([[data_stat[0],data_stat[1]]]).T, ecolor="black", capsize=5, color="black", zorder=10, ls='-')

ax.hlines(y=y_max[9], xmin=0.83, xmax=1.05, linewidth=1, color='black', linestyle='-.')

plt.text(0.84, y_max[8], r"PDF4LHC21", verticalalignment='center', horizontalalignment="left", fontsize=20)
plt.text(0.84, y_max[8]-gap, r"$m_t=171.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=15)
eb2=plt.errorbar(x=1.2720, y=y_max[8], fmt="^", xerr=np.array([[0.0731,0.0651]]).T, ecolor="black", capsize=5, color="black", zorder=10)
# eb2_pdf=plt.bar(x=1.2720, y=y_max[8], fmt="^", xerr=np.array([[0.0566,0.0566]]).T, ecolor="black", capsize=5, color="black", zorder=10, elinewidth=0)
# eb2[-1][0].set_linestyle('--')
rect = Rectangle((1.2720 - 0.0566, y_max[8] - 0.1), 0.0566+0.0566,0.2, linewidth=1, edgecolor='#474747', hatch='//', facecolor='none', zorder=9)
plt.gca().add_patch(rect)


plt.text(0.84, y_max[7], r"PDF4LHC21", verticalalignment='center', horizontalalignment="left", fontsize=20)
# plt.text(0.84, y_max[7]-gap, r"$m_t=172.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=15)
eb3=plt.errorbar(x=1.2379, y=y_max[7], fmt="^", xerr=np.array([[0.0712,0.0634]]).T, ecolor="black", capsize=5, color="black", zorder=10)
# eb3_pdf=plt.errorbar(x=1.2379, y=y_max[7], fmt="^", xerr=np.array([[0.0551,0.0551]]).T, ecolor="black", capsize=5, color="black", zorder=10, elinewidth=0)
# eb3[-1][0].set_linestyle('--')
rect = Rectangle((1.2379 - 0.0551, y_max[7] - 0.1), 0.0551+0.0551,0.2, linewidth=1, edgecolor='#474747', hatch='//', facecolor='none', zorder=9)
plt.gca().add_patch(rect)


plt.text(0.84, y_max[6], r"PDF4LHC21", verticalalignment='center', horizontalalignment="left", fontsize=20)
plt.text(0.84, y_max[6]-gap,r"$m_t=173.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=15)
eb4=plt.errorbar(x=1.2048, y=y_max[6], fmt="^", xerr=np.array([[0.0693,0.0617]]).T, ecolor="black", capsize=5, color="black", zorder=10)
# eb4=plt.errorbar(x=1.2048, y=y_max[6], fmt="^", xerr=np.array([[0.0537,0.0537]]).T, ecolor="black", capsize=5, color="black", zorder=10, elinewidth=0)
# eb4[-1][0].set_linestyle('--')
rect = Rectangle((1.2048 - 0.0537, y_max[6] - 0.1), 0.0537+0.0537,0.2, linewidth=1, edgecolor='#474747', hatch='//', facecolor='none', zorder=9)
plt.gca().add_patch(rect)


plt.text(0.84, y_max[5], r"CT18", verticalalignment='center', horizontalalignment="left", fontsize=20)
# plt.text(0.84, y_max[5]-gap, r"$m_t=172.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=15)
eb5=plt.errorbar(x=1.2661, y=y_max[5], fmt="^", xerr=np.array([[0.1024,0.1078]]).T, ecolor="black", capsize=5, color="black", zorder=10)
# eb5=plt.errorbar(x=1.2661, y=y_max[5], fmt="^", xerr=np.array([[0.0913,0.1031]]).T, ecolor="black", capsize=5, color="black", zorder=10, elinewidth=0)
# eb5[-1][0].set_linestyle('--')
rect = Rectangle((1.2661 - 0.0913, y_max[5] - 0.1), 0.0913+0.1031,0.2, linewidth=1, edgecolor='#474747', hatch='//', facecolor='none', zorder=9)
plt.gca().add_patch(rect)


plt.text(0.84, y_max[4], r"CT18A", verticalalignment='center', horizontalalignment="left", fontsize=20)
# plt.text(0.84, y_max[4]-gap, r"$m_t=172.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=15)
eb6=plt.errorbar(x=1.2237, y=y_max[4], fmt="^", xerr=np.array([[0.085,0.0783]]).T, ecolor="black", capsize=5, color="black", zorder=10)
# eb6=plt.errorbar(x=1.2237, y=y_max[4], fmt="^", xerr=np.array([[0.0645,0.0720]]).T, ecolor="black", capsize=5, color="black", zorder=10, elinewidth=0)
# eb6[-1][0].set_linestyle('--')
rect = Rectangle((1.2237 - 0.0645, y_max[4] - 0.1), 0.0645+0.0720,0.2, linewidth=1, edgecolor='#474747', hatch='//', facecolor='none', zorder=9)
plt.gca().add_patch(rect)


plt.text(0.84, y_max[3], r"MSHT20", verticalalignment='center', horizontalalignment="left", fontsize=20)
# plt.text(0.84, y_max[3]-gap, r"$m_t=172.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=15)
eb7=plt.errorbar(x=1.2340, y=y_max[3], fmt="^", xerr=np.array([[0.0571,0.0624]]).T, ecolor="black", capsize=5, color="black", zorder=10)
# eb7=plt.errorbar(x=1.2340, y=y_max[3], fmt="^", xerr=np.array([[0.0351,0.0539]]).T, ecolor="black", capsize=5, color="black", zorder=10, elinewidth=0)
# eb7[-1][0].set_linestyle('--')
rect = Rectangle((1.2340 - 0.0351, y_max[3] - 0.1), 0.0351+0.0539,0.2, linewidth=1, edgecolor='#474747', hatch='//', facecolor='none', zorder=9)
plt.gca().add_patch(rect)


plt.text(0.84, y_max[2], r"NNPDF4.0", verticalalignment='center', horizontalalignment="left", fontsize=20)
# plt.text(0.84, y_max[2]-gap, r"$m_t=172.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=15)
eb8=plt.errorbar(x=1.1748, y=y_max[2], fmt="^", xerr=np.array([[0.0498,0.0361]]).T, ecolor="black", capsize=5, color="black", zorder=10)
# eb8=plt.errorbar(x=1.1748, y=y_max[2], fmt="^", xerr=np.array([[0.0256,0.0209]]).T, ecolor="black", capsize=5, color="black", zorder=10, elinewidth=0)
# eb8[-1][0].set_linestyle('--')
rect = Rectangle((1.1748 - 0.0256, y_max[2] - 0.1), 0.0256+0.0209,0.2, linewidth=1, edgecolor='#474747', hatch='//', facecolor='none', zorder=9)
plt.gca().add_patch(rect)


plt.text(0.84, y_max[1], r"ATLASpdf21", verticalalignment='center', horizontalalignment="left", fontsize=20)
# plt.text(0.84, y_max[1]-gap, r"$m_t=172.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=15)
eb9=plt.errorbar(x=1.2440, y=y_max[1], fmt="^", xerr=np.array([[0.0767,0.0779]]).T, ecolor="black", capsize=5, color="black", zorder=10)
# eb9=plt.errorbar(x=1.2440, y=y_max[1], fmt="^", xerr=np.array([[0.0619,0.0713]]).T, ecolor="black", capsize=5, color="black", zorder=10, elinewidth=0)
# eb9[-1][0].set_linestyle('--')
rect = Rectangle((1.2440 - 0.0619, y_max[1] - 0.1), 0.0619+0.0713,0.2, linewidth=1, edgecolor='#474747', hatch='//', facecolor='none', zorder=9)
plt.gca().add_patch(rect)


plt.text(0.84, y_max[0], r"ABMP16", verticalalignment='center', horizontalalignment="left", fontsize=20)
# plt.text(0.84, y_max[0]-gap, r"$m_t=172.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=15)
eb10=plt.errorbar(x=1.1259, y=y_max[0], fmt="^", xerr=np.array([[0.0560,0.0473]]).T, ecolor="black", capsize=5, color="black", zorder=10)
# eb10=plt.errorbar(x=1.1259, y=y_max[0], fmt="^", xerr=np.array([[0.0378,0.0378]]).T, ecolor="black", capsize=5, color="black", zorder=10, elinewidth=0)
# eb10[-1][0].set_linestyle('--')
rect = Rectangle((1.1259 - 0.0378, y_max[0] - 0.1), 0.0378+0.0378,0.2, linewidth=1, edgecolor='#474747', hatch='//', facecolor='none', zorder=9)
plt.gca().add_patch(rect)

plt.legend(handles=[eb0, eb2, legend_rect],
           handler_map={type(eb2): HandlerErrorbar(xerr_size=3.2)},
           labels=['combined result', 'theory (total uncertainty)', r'PDF+$\alpha_{\mathrm{S}}$ uncertainty'],
           ncol=1, handletextpad=1.0, handlelength=6,
           loc='upper right', bbox_to_anchor=(1.02, 0.9), frameon=False, fontsize=18)

# plt.text(1.2, 11.8, r"(NNLO QCD + NNLL, inner uncert.: PDF+$\alpha_{\mathrm{S}}$)", verticalalignment='center', horizontalalignment="left", fontsize=15)

#ATLAS label
hep.atlas.label(label="", rlabel=r"$\sqrt{s}=13.6\,$TeV,$ \: 29 \: \mathrm{fb^{-1}}$", data=True, loc=4, fontsize=22)
# plt.text(0.97, 14.0, r"Internal", verticalalignment='center', horizontalalignment="left", fontsize=24)

plt.xticks(fontsize=25)
# plt.text(7.0, 12.0, r"If not explicitly reported, $m_t=172.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=15)
# plt.text(7.0, 12.0, r"If not explicitly reported, $m_t=172.5$ GeV", verticalalignment='center', horizontalalignment="left", fontsize=15)

#adapt this to have nice plot aspect ratio
plt.xlim(0.80, 1.5)
plt.ylim(0, 15)
fig.savefig("Xsec_ratio_vs_Theory.pdf")
fig.savefig("Xsec_ratio_vs_Theory.png")
