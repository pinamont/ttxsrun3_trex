import matplotlib. pyplot as plt
import numpy as np
import mplhep as hep
hep.style.use(hep.style.ATLAS)

#values
#measurement
data_nom=0.400
data_stat=[0.006, 0.006] #first is down, second is up
data_syst=[np.sqrt(data_stat[0]**2+0.016**2), np.sqrt(data_stat[1]**2+0.017**2)] #uncertainty including stat and syst
data_lumi=[np.sqrt(data_syst[0]**2+0.005**2), np.sqrt(data_syst[1]**2+0.005**2)] #total uncertainty including stat, syst and lumi

#theory values
theory_nom = [0.4232]
theory_err = [[0.0154],[0.0154]]

fig, ax = plt.subplots()
#set labels
ax.set_xlabel(r"$\sigma_{t\bar{t}} / \sigma_{Z}$", loc="right", va="top")
ax.set_yticks([])
[x.set_linewidth(1.5) for x in ax.spines.values()]
ax.xaxis.set_tick_params(width=1.5)
ax.axvspan(data_nom-data_lumi[0], data_nom+data_lumi[1], color='limegreen', label="+luminosity")
ax.axvspan(data_nom-data_syst[0], data_nom+data_syst[1], color='cyan', label="+exp. syst.")
ax.axvspan(data_nom-data_stat[0], data_nom+data_stat[1], color='yellow', label="statistics")
ax.axvline(data_nom, color="red")
plt.legend(*(
    [ x[i] for i in [2,1,0] ]
    for x in plt.gca().get_legend_handles_labels()
), handletextpad=0.75, loc='upper right', frameon=False)

#theory
plt.text((theory_nom[0]-theory_err[0][0])*0.905, 1, "PDF4LHC21", verticalalignment='center', horizontalalignment="right")
plt.errorbar(x=theory_nom, y=1, marker="x", xerr=theory_err, ecolor="black", capsize=2, color="black", zorder=10)
#ATLAS label
hep.atlas.label(label="Internal", rlabel=r"$\sqrt{s}=13.6\,$TeV,$\;1.2\,\mathrm{fb^{-1}}$", data=True, loc=4)

#adapt this to have nice plot aspect ratio
plt.xlim(0.35, 0.45)
plt.ylim(0.5, 2)
fig.savefig("Xsec_ratio_vs_Theory.pdf")
