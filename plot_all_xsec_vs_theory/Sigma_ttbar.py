#!/usr/bin/python3
# ecmplot.py - extracted xsec vs ECM plot from ttx6/xpplots.py
# Richard Hawkings, 23/2/23
# added required library classes from rxcplot

import math
import ROOT
from ROOT import TFile,TH1F,gPad,TGraph,TPave,TGraphErrors,TGraphAsymmErrors,TMultiGraph,TPad,TLatex,TLine,TStyle,TCanvas,TMarker,gROOT
from array import array

gROOT.Reset()

def setAtlasStyle():
    "ATLAS style setup"
    # translation of Tancredi's ATLASstyle macro into python
    # note you must keep the TStyle instance this returns to preserve
    # the lifetime of the style
    ats=TStyle("ATLAS","Atlas style")
    ats.SetFrameBorderMode(0)
    ats.SetCanvasBorderMode(0)
    ats.SetPadBorderMode(0)
    ats.SetPadColor(0)
    ats.SetCanvasColor(0)
    ats.SetStatColor(0)
    ats.SetPaperSize(20,26)
    ats.SetPadTopMargin(0.03)
    ats.SetPadRightMargin(0.03)
    ats.SetPadBottomMargin(0.16)
    ats.SetPadLeftMargin(0.16)
    # use large fonts
    font=42
    tsize=0.05
    ats.SetTextFont(font)
    ats.SetTextSize(tsize)
    ats.SetLabelSize(tsize,"x")
    ats.SetTitleSize(tsize,"x")
    ats.SetLabelSize(tsize,"y")
    ats.SetTitleSize(tsize,"y")
    
    ats.SetLabelFont(font,'x')
    ats.SetTitleFont(font,'x')
    ats.SetLabelFont(font,'y')
    ats.SetTitleFont(font,'y')
    ats.SetLabelFont(font,'z')
    ats.SetTitleFont(font,'z')
    ats.SetTitleXOffset(1.4)
    ats.SetTitleYOffset(1.8)
    # lines and markers
    ats.SetMarkerStyle(20)
    ats.SetMarkerSize(0.6) # was 0.8 then 0.7
    # different - line width of 1 not 2
    ats.SetHistLineWidth(1)
    ats.SetLineStyleString(2,'[12 12]')
    # do not display standard histogram decorations
    ats.SetOptTitle(0)
    ats.SetOptStat(0)
    ats.SetOptFit(0)
    ats.SetPadTickX(1)
    ats.SetPadTickY(1)
    gROOT.SetStyle("ATLAS")
    gROOT.ForceStyle()
    return ats

class msPlot:
    "Class for making plots from multiple input samples"
    def __init__(self,samples=[],labels=[],colour=False):
        "Setup - supply list of samples and associated labels (parallel lists)"
        if colour: print("Color mode enabled")
        self.samples=[]
        if (len(samples)!=len(labels)):
            print("ERROR: sample and label sizes do not match")
        for (s,l) in zip(samples,labels):
            print("Attaching file %s with label %s" % (s,l))
            self.samples+=[(TFile(s),l)]
        # set ATLAS style for plot
        self.atlasStyle=setAtlasStyle()
        self.canvas=None
        self.colour=colour
        if colour: print("Color mode enabled")
        self.reset()
        self.ipad=0
        self.nolabel=False

    def reset(self):
        "Reset graphics attributes, delete existing histograms"
        self.keeplist=[]
        self.ymin=None
        self.ymax=None
        self.ndivxrat=None
        self.ndivyrat=503
        self.axlabels=None

    def initCanvas(self,id,name,x1,y1,x2,y2,div1,div2):
        "Setup the canvas and divide it according to the parameters"
        self.canvas=TCanvas(id,name,x1,y1,x2,y2)
        self.canvas.Divide(div1,div2)
        self.ipad=0

    def connectCanvas(self,msRef):
        "Connect canvas to already existing msPlot object"
        self.canvas=msRef.canvas
        self.ipad=msRef.ipad

    def setYMinMax(self,ymin=None,ymax=None):
        "Set histogram ymin and ymax"
        self.ymin=ymin
        self.ymax=ymax

    def getFile(self,i):
        "return TFile object for sample i"
        return (self.samples[i])[0]

    def nextPad(self):
        "Advance to next pad"
        self.ipad+=1
        self.canvas.cd(self.ipad)

    def dumpPlot(self,file='c1',wait=True):
        "Dump the plots to eps and png files, wait for input before returning"
        print("Dumping plots to file %s.pdf" % file)
        self.canvas.Update()
        #self.canvas.Print('%s.ps' % file)
        #self.canvas.Print('%s.eps' % file)
        self.canvas.Print('%s.pdf' % file)
        #self.canvas.Print('%s.png' % file)
        if wait:raw_input("Press return when done")

    def labelText(self,x,y,text,tsize=None):
        "Simple text label on plot"
        txt=TLatex()
        txt.SetNDC()
        if tsize is not None: txt.SetTextSize(tsize)
        txt.DrawLatex(x,y,text)

    def labelTextA(self,x,y,text,tsize=None):
        "Simple text label without NDC switch"
        txt=TLatex()
        if tsize:txt.SetTextSize(tsize)
        txt.DrawLatex(x,y,text)

    def lineText(self,x,y,len,style,text,width=1,style2=None,yofs=0.02):
        "Draw a key line of given style, with associated text"
        txt=TLatex()
        txt.SetNDC()
        txt.DrawLatex(x,y,text)
        line=TLine()
        line.SetLineWidth(width)
        if (self.colour or style2 is not None):
            if style2 is not None:
                line.SetLineStyle(style2)
            else:
                line.SetLineStyle(1)
            line.SetLineColor(style)
        else:
            line.SetLineColor(1)
            line.SetLineStyle(style)
        line.DrawLineNDC(x-0.02-len,y+yofs,x-0.02,y+yofs)

    def keyText(self,x,y,key,text,col=None):
        "Draw a polymarker of given style, with associated text"
        txt=TLatex()
        txt.SetNDC()
        txt.DrawLatex(x,y,text)
        marker=TMarker(x-0.03,y+0.02,key+19)
        marker.SetNDC()
        if self.colour:
            marker.SetMarkerColor(key)
        if col:
            marker.SetMarkerColor(col)
        marker.Draw()
        self.keeplist+=[marker]

    def keyText2(self,x,y,key,col,text,xofs=0.03):
        "Draw a polymarker of given style and colour, with associated text"
        txt=TLatex()
        txt.SetNDC()
        txt.DrawLatex(x,y,text)
        # explicit seting of marker type
        marker=TMarker(x-xofs,y+0.02,key)
        marker.SetNDC()
        marker.SetMarkerColor(col)
        marker.Draw()
        self.keeplist+=[marker]

    def hBoxText(self,x,y,bsize,style,text,border=True,style2=None,alpha=None,tsize=0.06,clinecol=None):
        "Draw a key line of given style, with associated text"
        txt=TLatex()
        txt.SetNDC()
        # this is needed for ttx6 XSec vs PDF plot
        # txt.SetTextSize(tsize)
        txt.DrawLatex(x,y,text)
        x2=x-0.3*tsize
        x1=x2-bsize
        y1=y
        y2=y+0.5*tsize
        box=TPave(x1,y1,x2,y2,1,"NDC")
        # box=TBox(x1,y1,x2,y2)
        if (self.colour or style<0):
            # need fill style 1001 to get PDF right (tip from Andreas H)
            if style2:
                box.SetFillStyle(style2)
            else:
                box.SetFillStyle(1001)
            if alpha:
                box.SetFillColorAlpha(abs(style),alpha)
            else:
                box.SetFillColor(abs(style))
                # box.SetLineColor(abs(style))
            if not border:
                    box.SetLineColor(abs(style))
        else:
            box.SetLineColor(1)
            box.SetFillColor(1)
            box.SetFillStyle(style)
        box.Draw()
        self.keeplist+=[box]
        if clinecol is not None:
            xd=(x1+x2)/2.
            tl=TLine(xd,y1,xd,y2)
            tl.SetLineColor(clinecol)
            tl.SetLineWidth(2)
            tl.DrawLineNDC(xd,y1,xd,y2)
            self.keeplist+=[tl]

    def hBoxTextR(self,x,y,bsize,style,text,border=True,style2=None):
        "Draw a key line of given style, with ass text, version for ratio"
        txt=TLatex()
        txt.SetNDC()
        txt.SetTextSize(0.12)
        txt.DrawLatex(x,y,text)
        tsize=0.1
        x2=x-0.2*tsize
        x1=x2-bsize
        y1=y+0.2*tsize
        y2=y+0.7*tsize
        box=TPave(x1,y1,x2,y2,1,"NDC")
        # box=TBox(x1,y1,x2,y2)
        if (self.colour or style<0):
            # need fill style 1001 to get PDF right (tip from Andreas H)
            if style2:
                box.SetFillStyle(style2)
            else:
                box.SetFillStyle(1001)
            box.SetFillColor(abs(style))
            if not border:
                box.SetLineColor(abs(style))
        else:
            box.SetLineColor(1)
            box.SetFillColor(1)
            box.SetFillStyle(style)
        box.Draw()
        self.keeplist+=[box]

    def lineDeco(self,x,y,len,style,colour,width=1):
        "Draw decoration line in same style as lineText"
        line=TLine()
        line.SetLineWidth(width)
        line.SetLineStyle(style)
        line.SetLineColor(colour)
        line.DrawLineNDC(x-0.02-len,y+0.03,x-0.02,y+0.03)

    def bareLine(self,x1,y1,x2,y2,colour,style):
        "Draw a simple line in histogram coordinate system"
        line=TLine()
        line.SetLineStyle(style)
        line.SetLineColor(colour)
        line.DrawLine(x1,y1,x2,y2)
        

    def findKeep(self,name):
        "Find the object of given name in the keeplist"
        for tobj in self.keeplist:
            print(tobj.GetName())
            if tobj.GetName()==name: return tobj
        return None

    def plotTH1(self,hname,axisx="",axisy="",locx=0,locy=0,sopt="",norm=False):
        "Plot a histogram for each file in the sample"
        print("Drawing histogram %s for all files" % hname)
        opt=sopt
        first=True
        istyle=1
        for (s,l) in self.samples:
            hptr=s.Get(hname)
            print("Sample %s mean %f" % (l,hptr.GetMean()))
            hptr.SetLineStyle(istyle)
            hptr.SetMarkerStyle(19+istyle)
            if (self.colour):
                hptr.SetMarkerColor(istyle)
            if norm: hptr.SetNormFactor(1.)
            if (first):
                if (axisx!=""):
                    hptr.SetXTitle(axisx)
                if (axisy!=""):
                    hptr.SetYTitle(axisy)
                # histogram limits
                if self.ymin is not None:
                    hptr.SetMinimum(self.ymin)
                if self.ymax is not None:
                    hptr.SetMaximum(self.ymax)
                hptr.Draw(opt)
                if (len(sopt)>0): opt+=","
                opt+="SAME"
                first=False
            else:
                hptr.Draw(opt)
            istyle+=1
        # put in labels if needed
        if (locx!=0):
            y=locy
            istyle=1
            for (s,l) in self.samples:
                self.keyText(locx,y,istyle,l)
                y-=0.05
                istyle+=1

    def ATLAS_LABEL(self,x,y,tsize=0.05,xofs=0.135,alignv=False):
        if self.nolabel: return
        l=TLatex()
        l.SetNDC()
        l.SetTextFont(72)
        l.SetTextColor(1)
        l.SetTextSize(tsize)
        l.DrawLatex(x,y,"ATLAS")
        l2=TLatex()
        l2.SetNDC()
        l2.SetTextColor(1)
        l2.SetTextFont(42)
        l2.SetTextSize(tsize*0.75)
        l2.SetTextSize(tsize*1.)
        if alignv:
            # l2.DrawLatex(x + 0.2,y,"Preliminary")
            # l2.DrawLatex(x,y-0.05,"Internal")
            pass
        else:
            # l2.DrawLatex(x+xofs,y,"Preliminary")
            # l2.DrawLatex(x+xofs,y,"Internal")
            pass
        self.keeplist+=[l,l2]

    def ATLAS_SIMLABEL(self,x,y,tsize=0.05,xofs=0.15,yofs=0.15):
        if self.nolabel: return
        l=TLatex()
        l.SetNDC()
        l.SetTextFont(72)
        l.SetTextColor(1)
        # l.SetTextSize(tsize)
        l.DrawLatex(x,y,"ATLAS")
        l2=TLatex()
        l2.SetNDC()
        l2.SetTextColor(1)
        l2.SetTextFont(62)
        l2.SetTextSize(tsize*0.9)
        # temporary remove labels
        l2.DrawLatex(x+xofs,y,"Simulation")
        #l2.DrawLatex(x+xofs,y,"Simulation Internal")
        #l2.DrawLatex(x+xofs,y,"Simulation Preliminary")
        #l3=TLatex()
        #l3.SetNDC()
        #l3.SetTextColor(1)
        #l3.SetTextFont(62)
        #l3.SetTextSize(tsize*0.8)
        # l2.DrawLatex(x+xofs,y,"Preliminary")
        # l3.DrawLatex(x,y-yofs,"Internal")
        self.keeplist+=[l,l2] #,l3]

class ECMPlot:
    "Class to make plot of cross-section vs energy using ref parameterisation"
    # taken from ttx5/xppplots.py, adapted to include 5.02 TeV
    def __init__(self,ofilen=""):
        self.p=msPlot([],[],True)
        # file output
        if ofilen!="":
            print("Duplicating output to %s" % ofilen)
            ofile=open(ofilen,'w')
        else:
            ofile=None
        # parameterisation of cross-section from ref INT note, Table 13
        # for mtop=172.5 GeV
        # for each PDF, data for up PDF, central, down PDF
        # parameters a0-a6
        mpdfdata={
            'PDF':((-20.724773894521217, 0.005993240418493258, 4.829418232046296e-06, -0.003971448964676232, 0.0022414448414565464, 1.656308199310481e-06, -1.8167738211881228e-06),
                    (-22.085907601757796, 0.0061504927118894125, 4.702163809960988e-06, -0.004248768986411243, 0.002318532127058522, 1.6873979068754077e-06, -1.822033201272403e-06),
                    (-23.45373968881153, 0.006304407807693068, 4.5751899181471736e-06, -0.004524123857115607, 0.002397296228410731, 1.7180583310173098e-06, -1.8270490640088957e-06)),
            'PDFscale':((-54.446313235497705, -0.02109412624947176, 6.942284264373742e-06, 0.00243241311910668, 0.01047315856174462, -9.428365242029383e-07, -3.865449311563695e-07),
                    (-22.085907601757796, 0.0061504927118894125, 4.702163809960988e-06, -0.004248768986411243, 0.002318532127058522, 1.6873979068754077e-06, -1.822033201272403e-06),
                    (-51.36347456598601, -0.019875628789039448, 6.534116919878757e-06, 0.0022830838454938884, 0.009873989788118638, -8.888422219104606e-07, -3.6443051158882606e-07)),
            'PDF4LHC21+scale': ((0, 0, 0, 0, 0, 0, 0, 0),
                    (-22.085907601757796, 0.0061504927118894125, 4.702163809960988e-06, -0.004248768986411243, 0.002318532127058522, 1.6873979068754077e-06, -1.822033201272403e-06),
                    (0, 0, 0, 0, 0, 0, 0, 0)),
            }
        mscaledata={
            'PDFscale':((-54.446313235497705, -0.02109412624947176, 6.942284264373742e-06, 0.00243241311910668, 0.01047315856174462, -9.428365242029383e-07, -3.865449311563695e-07),
                    (-22.085907601757796, 0.0061504927118894125, 4.702163809960988e-06, -0.004248768986411243, 0.002318532127058522, 1.6873979068754077e-06, -1.822033201272403e-06),
                    (-51.36347456598601, -0.019875628789039448, 6.534116919878757e-06, 0.0022830838454938884, 0.009873989788118638, -8.888422219104606e-07, -3.6443051158882606e-07)),
        }

        # energy and luminosity labels
        self.mlumi=['29','140','20.2','4.6','0.26']
        self.mlecm=array('f',[13.6,13.,8.,7.,5.02])
        self.necm=len(self.mlecm)

        # group for each measurement, corresponding to markers
        # group 0 is e-mu, 1 dilepton, 2 l+jets, 3 combination
        self.ngrp=4
        self.mkgrp=[20,22,23,21]
        self.colgrp=[1,2,797,6]

        # data results for 13, 8, 7, 5 TeV 
        # updated 28/7/21 for new 5 TeV result with final JES
        self.mecmplot=[]  # Ecm with offset for plot
        self.mecm=[]
        self.mxsec=[]
        self.mxrel=[]
        self.mxgrp=[]
        # add measurements (ecm, group, xsec, relerror)
        # dileptons
        self.addMeas(13.6,0,850.0,0.032)
        self.addMeas(13.,0,829.0,0.0186)
        self.addMeas(8. ,0,242.94,0.0321)
        self.addMeas(7. ,0,182.90,0.0353)
        self.addMeas(5.02,1,65.7,0.0749)
        # lepton+jets
        self.addMeas(13.,2,830.,0.046)
        self.addMeas(8., 2,248.3,0.057)
        self.addMeas(7., 2,168.5,0.042)
        # no 7 TeV as from ATLAS-CONF-2012-131, preliminary
        self.addMeas(5.02,2,68.2,0.045) # unblinded 5 TeV 
        # combination
        self.addMeas(5.02,3,67.5,0.039) # unblinded 5 TeV


        #self.mecm=array('f',[13.,8.,7.,5.02])
        #self.mxsec=array('f',[826.4,242.94,182.90,65.7])
        #self.mxrel=array('f',[0.0238,0.0321,0.0353,0.0749])
        
        # transform results to arrays for input into TGraphErrors
        self.nmeas=len(self.mecm)
        self.mecmplot=array('f',self.mecmplot)
        self.mxsec=array('f',self.mxsec)
        self.mxrel=array('f',self.mxrel)


        # index of scan positions for each measurement
        self.idx=self.nmeas*[-1]

        # construct energy scan grid, down to 4 TeV
        self.emin=4.
        self.emax=14.5
#        self.npt=50+1
        self.npt=int(10.*(self.emax-self.emin)+0.1)+1
        # vector of centre of mass energies (in TeV)
        self.vecm=array('f',self.npt*[0.])
        for ie in range(self.npt):
            ecm=self.emin+(self.emax-self.emin)*float(ie)/(self.npt-1.)
            # hack to get 5.02 TeV
            if abs(ecm-5.)<0.1: ecm+=0.02
            self.vecm[ie]=ecm
            
            # check if this point is one of the measurements
            for im in range(self.nmeas):
                if abs(ecm-self.mecm[im])<1.E-3:
                    self.idx[im]=ie

        # construct QCD scale error parameterisations
        self.scalemap={}
        for (iscal,scaledata) in mscaledata.items():
            pdf=EPred(iscal,self.vecm)
            pdf.fillPred(scaledata)
            pdf.printXSec(ofile)
            self.scalemap[iscal]=pdf
             
        # construct map of PDF name to EPred objects
        self.predmap={}
        for (ipdf,pdfdata) in mpdfdata.items():
            pdf=EPred(ipdf,self.vecm)
            pdf.fillPred(pdfdata)
            pdf.printXSec(ofile)
            self.predmap[ipdf]=pdf
        self.pdfsets=self.predmap.keys()
        print("Initialised for PDF sets",self.pdfsets)

        # create the PDF4LHC21 set using the envelope
        # done before adding the scale uncertainties
        pdf4lhc=EPred('PDF4LHC21',self.vecm)
        self.predmap['PDF4LHC21']=pdf4lhc
        for ie in range(self.npt):
            # find the min/max xsec for each
            xmin=1.E9
            xmax=-1.E9
            for pdfset in ['PDF']:
                ipdf=self.predmap[pdfset]
                if ipdf.xsecup(ie)>xmax: xmax=ipdf.xsecup(ie)
                if ipdf.xsecdn(ie)<xmin: xmin=ipdf.xsecdn(ie)
            # central value of PDF4LHC21 is midpoint
            pdf4lhc.set(ie,(xmin+xmax)/2.,xmin,xmax)
        pdf4lhc.printXSec(ofile)

        # get combined PDF and scale unc
        nnpas=EPred('PDF4LHC21+scale',self.vecm)
        self.predmap['PDF4LHC21+scale']=nnpas
        nnp=self.predmap['PDFscale']
        nnpnoas=self.predmap['PDF']
        for ie in range(self.npt):
            # add the quadrature of PDF and PDF scale unc
            eup=math.sqrt(nnp.dxup[ie]**2+nnpnoas.dxup[ie]**2)
            edn=math.sqrt(nnp.dxdn[ie]**2+nnpnoas.dxdn[ie]**2)
            xcent=nnp.xcent[ie]
            nnpas.set(ie,xcent,xcent-edn,xcent+eup)
        nnpas.printXSec(ofile)

        # add scale uncertianties for all PDFs
        print("Skipped: Adding scale uncertainties")
       # for (ipdf,pdf) in self.predmap.items():
       #     # add appropriate scale 
       #     if ipdf in ['CT14','NNPDF3.1','NNPDF3.1noas','NNPDF3.1_notop']:
       #         continue
#      #          pdf.addError(self.scalemap['ScaleCT14'])
       #     else:
       #         continue
       #         pdf.addError(self.scalemap['PDFscale'])
       #     pdf.printXSec(ofile)
       # # close output file if needed
        if ofile:
            ofile.close()

    def addMeas(self,ecm,igrp,xsec,erel):
        "Add one measurement"
        # offsets -ve for e-mu and dileptons, +ve for l+jets, 0 for comb
        offset=[-0.07,-0.07,0.07,0][igrp]
        self.mecmplot+=[ecm+offset]
        self.mecm+=[ecm]
        if igrp>=self.ngrp:
            raise Exception('Request to add measurement for undefined group %i %i' % (igrp,self.ngrp))
        self.mxgrp+=[igrp]
        self.mxsec+=[xsec]
        self.mxrel+=[erel]

    def plot(self,pub=True):
        "Make the plot of ttbar x-sec vs ECM - new version with run-2 PDFs too"
        self.p.initCanvas("c1","conf",0,0,600,560,1,1)
        # colour and hatching for PDF ratios
        # colmap=[2,3,4]
        self.colmap=[2,4,3]
        self.hatchmap=[3354,3345,3359] # ,3305]

        # setup pads for main plot and ratio
        ratsize=0.42  # size reserved for ratios
        # reduction factor in dimensions for pad2, compensating for axis space
        rats=0.68  
        self.p.nextPad()
        pad1=TPad('c1p1',"p1",0.,ratsize,1.,1.)
        pad1.Draw()
        # no small space between pads
        pad1.SetBottomMargin(0.01)
        pad1.SetLeftMargin(0.14)
        pad1.SetRightMargin(0.03)
        # pad2 for PDFs
        pad2=TPad("c1p2","p2",0.,0.,1.,ratsize) #this modifies the lower panel
        pad2.SetTopMargin(0.)
        pad2.SetBottomMargin(0.4)
        pad2.SetLeftMargin(0.14)
        pad2.SetRightMargin(0.03)
        pad2.Draw()
        pad1.cd()
        pad1.SetLogy()

        # draw frame for main plot
        hax=TH1F('XvECM','XvECM',self.npt,self.emin,self.emax)
        hax.SetMinimum(30.)
        hax.SetMaximum(1100.)
        hax.GetYaxis().SetTitleSize(0.05)
        #hax.GetYaxis().SetLabelSize(0.06)
        hax.GetYaxis().SetTitleOffset(1.1)
        hax.SetYTitle('Inclusive t#bar{t} cross-section  #sigma_{t#bar{t}} [pb]')
        hax.Draw('HIST')
        self.p.keeplist+=[pad1,pad2,hax]
        self.p.keeplist+=[pad1,pad2,hax]

        # draw PDF4LHC21 prediction
        tgplhc=self.predmap['PDF4LHC21'].makeTGA()
        tgplhc.SetFillColor(7)
        tgplhc.SetLineColor(2)
        tgplhc.Draw('3')
        tgplhc.Draw('lx,SAME')
        self.p.keeplist+=[tgplhc]

        # create TGraphErrors for measurements and ratios to prediction
        merr=array('f',self.nmeas*[0.])
        # calculate absolute errors on measurements
        for im in range(self.nmeas):
            merr[im]=self.mxrel[im]*self.mxsec[im]
            print("Measurement %i (%2i) %6.2f %6.2f+- %5.2f (%5.2f %%)" % (im,self.idx[im],self.mecm[im],self.mxsec[im],merr[im],100.*self.mxrel[im]))
        tgdata=self.makeTGMeas('Res',self.mxsec,merr)
        for i in tgdata:
            i.Draw('SAME,PZ')
        hax.Draw('AXIS,SAME')
        self.p.keeplist+=[tgdata]
        
        # labels for main plot
        x0=0.68
        y0=0.6
        if pub:
            self.p.ATLAS_LABEL(0.2,0.85,0.06,0.11)
        self.p.atlasStyle.SetMarkerSize(0.8)
        for i in range(self.ngrp):
            txt=['e#mu + b-tagged jets','#font[12]{ l l} + b-tagged jets','#font[12]{ l} + jets','combined'][i]
            self.p.keyText2(0.22,0.78-0.07*i,self.mkgrp[i],self.colgrp[i],txt,0.015)
        for ip in range(self.necm):
            if self.mlecm[ip]>13.1:
                self.p.labelText(x0,y0-0.07*(ip+1),'#sqrt{s} = %0.1f TeV, %s fb^{ -1}' % (self.mlecm[ip],self.mlumi[ip]))
            elif ip!=self.necm-1:
                self.p.labelText(x0,y0-0.07*(ip+1),'#sqrt{s} = %0.0f TeV, %s fb^{ -1}' % (self.mlecm[ip],self.mlumi[ip]))
            else:
                # special formatting for 5 TeV
                self.p.labelText(x0,y0-0.07*(ip+1),'#sqrt{s} = %0.2f TeV, %s fb^{ -1}' % (self.mlecm[ip],self.mlumi[ip]))

        # theory labels
        xth=0.34
        yth=0.2
        self.p.hBoxText(xth,yth,0.04,7,'NNLO+NNLL (pp)',False)
        self.p.lineDeco(xth+0.001,yth-0.018,0.04,1,2)
        self.p.labelText(xth,yth-0.06,'Czakon, Fiedler, Mitov, PRL 110 (2013) 252004')
        self.p.labelText(xth,yth-0.12,'m_{t}=172.5 GeV, PDF+ #alpha_{S} uncertainties from PDF4LHC21')

        #self.p.labelText(0.55,0.85,'August 2023')

        # ratio plot, data and run-2 PDFs wrt CT14
        pad2.cd()
        self.drawRatios('PDF4LHC21','PDFscale', ['PDF4LHC21', 'PDF4LHC21+scale'], rats)


    def drawRatios(self,refname,scalename,pdflist,rats=1.,rlim=0.14):
        "Draw pad with ratios of several PDFs and data to reference"
        npdf=len(pdflist)
        refpdf=self.predmap[refname]
        refscale=self.scalemap[scalename]
        hrat=TH1F('RXvECM_%s' % refname,'RXvECM ref %s' % refname,self.npt,self.emin,self.emax)
        hrat.SetMinimum(1.-rlim)
        hrat.SetMaximum(1.+rlim)
        hrat.SetYTitle('Ratio wrt %s  ' % refname)
        hrat.SetXTitle('#sqrt{s} [TeV]')
        hrat.GetXaxis().SetTitleSize(0.105)
        hrat.GetXaxis().SetLabelSize(0.105)
        hrat.GetXaxis().SetLabelOffset(0.03)
        hrat.GetXaxis().SetTitleOffset(1.2)

        hrat.GetYaxis().SetTitleSize(0.11*rats)
        # hrat.GetYaxis().CenterTitle()
        hrat.GetYaxis().SetLabelSize(0.12*rats)
        hrat.GetYaxis().SetTitleOffset(0.49/rats)
        hrat.Draw('HIST')
        self.p.keeplist+=[hrat]

        # create and draw band for each PDF
        for ip in range(npdf):
            ipdf=pdflist[ip]
            pdfband=self.predmap[ipdf].makeRatioRef(refpdf)
            pdfsigma=self.predmap[ipdf].makeSigmaBands(refpdf)
            if ip<2:
                pdfband.SetFillStyle(self.hatchmap[ip])
                pdfband.SetFillColor(self.colmap[ip])
            else:
                pdfband.SetFillColorAlpha(self.colmap[ip],0.25)
            pdfband.Draw('3,SAME')
            for ib in range(2):
                pdfsigma[ib].SetLineColor(self.colmap[ip])
                pdfsigma[ib].Draw('SAME,L')
            self.p.keeplist+=[pdfband,pdfsigma]
            

        # create and draw line band for scale uncertainty
        scalesigma=refscale.makeSigmaBands(refscale)
        for ib in range(2):
            scalesigma[ib].SetLineStyle(2)
            scalesigma[ib].SetLineWidth(3)
            scalesigma[ib].Draw('L,SAME')
        self.p.keeplist+=[scalesigma]
            
        # create and draw ratio of data to reference
        mrat=array('f',self.nmeas*[0.])
        merr=array('f',self.nmeas*[0.])
        for im in range(self.nmeas):
            ie=self.idx[im]
            mrat[im]=self.mxsec[im]/refpdf.xsec(ie)
            merr[im]=self.mxrel[im]*mrat[im]
        tgdata=self.makeTGMeas('Ratio',mrat,merr)
        for i in tgdata:
            i.Draw('SAME,PZ')
        self.p.keeplist+=[tgdata]
        hrat.Draw('AXIS,SAME')

        # labels for PDF and scale
        tsize=0.12*rats
        for ip in range(npdf):
            if ip<2: #modify to have the PDF+QCD label first
                if ip==0:
                    ipreplace=1
                    self.hBoxTextSp(0.34+0.18*ipreplace,0.89,0.04,tsize,self.colmap[ip],pdflist[ip],True,self.hatchmap[ipreplace])
                if ip==1:
                    ipreplace=0
                    self.hBoxTextSp(0.26+0.18*ipreplace,0.89,0.04,tsize,self.colmap[ip],pdflist[ip],True,self.hatchmap[ipreplace])
#                self.hBoxTextSp(0.3+0.18*ip,0.89,0.04,tsize,self.colmap[ip],pdflist[ip],True,self.hatchmap[ip])
            else:
                self.hBoxTextSp(0.3+0.16*ip,0.89,0.04,tsize,self.colmap[ip],pdflist[ip],False,alpha=0.25)
        self.hBoxTextSp(0.3+0.21*npdf,0.89,0.04,tsize,-1,'QCD scales only')
        box=self.p.keeplist[-1]
        box.SetLineStyle(3)
        box.SetFillStyle(0)

    def makeTGMeas(self,name,vals,errs):
        "Make list of TGraphs of measurement+errors, grouped according to type"
        # do not use TMultiGraph for this, as it does not seem to respect
        # SAME option when plotted
        #tgm=TMultiGraph(name,name)
        tgm=[]
        for igrp in range(self.ngrp):
            xval=[]
            yval=[]
            yerr=[]
            for i in range(self.nmeas):
                if self.mxgrp[i]==igrp:
                    xval+=[self.mecmplot[i]]
                    yval+=[vals[i]]
                    yerr+=[errs[i]]
            n=len(xval)
            if n>0:
                tg=TGraphErrors(n,array('f',xval),array('f',yval),array('f',n*[0.001]),array('f',yerr))
                tg.SetMarkerStyle(self.mkgrp[igrp])
                tg.SetMarkerColor(self.colgrp[igrp])
                tg.SetLineColor(self.colgrp[igrp])
                tg.SetMarkerSize(0.8)
                #tgm.Add(tg,'PZ')
                tgm+=[tg]
        return tgm

    def hBoxTextSp(self,x,y,bsize,tsize,style,text,border=True,style2=None,alpha=None):
        "Draw a key line of given style, with associated text, special version"
        txt=TLatex()
        txt.SetNDC()
        txt.SetTextSize(tsize*0.7)
        txt.DrawLatex(x,y,text)
        x2=x-0.01
        x1=x2-bsize
        y1=y
        y2=y+0.5*tsize
        box=TPave(x1,y1,x2,y2,1,"NDC")
        # box=TBox(x1,y1,x2,y2)
        if (self.p.colour or style<0):
            # need fill style 1001 to get PDF right (tip from Andreas H)
            if style2:
                box.SetFillStyle(style2)
            else:
                box.SetFillStyle(1001)
            if alpha:
                box.SetFillColorAlpha(abs(style),alpha)
            else:
                box.SetFillColor(abs(style))
                # box.SetLineColor(abs(style))
                if style<0:
                    box.SetLineStyle(2)
                    box.SetLineWidth(3)
            if not border:
                box.SetLineColor(abs(style))
        else:
            box.SetLineColor(1)
            box.SetFillColor(1)
            box.SetFillStyle(style)
        box.Draw()
        self.p.keeplist+=[box]


class EPredPar:
    "Top cross-section vs energy using INT note parameterisation"
    def __init__(self,predpars):
        "Initialise, given predictions for up, central down a0-a6"
        self.npred=len(predpars)
        self.pars=[]
        for ip in range(self.npred):
            self.pars+=[array('f',predpars[ip])]

    def xsec(self,ecm,ip):
        "Return cross-section for given ECm and parameterisation number"
        p=self.pars[ip]
        x=math.log(ecm/14000.)
        xx=x*x
        return p[0]+ecm*(p[1]+p[3]*x+p[4]*xx)+ecm*ecm*(p[2]+p[5]*x+p[6]*xx)

class EPred:
    "Top cross-section vs energy, stored as array vs energy points with errors"
    def __init__(self,name,xevals):
        "Create representation with given number of points and min/max"
        self.name=name # PDF name
        self.xevals=xevals  # energy array
        self.npt=len(self.xevals)
        # central value and positive/negative errors
        self.xcent=array('f',self.npt*[0.])
        self.dxup=array('f',self.npt*[0.])
        self.dxdn=array('f',self.npt*[0.])

    def set(self,ie,xcent,xdn,xup):
        "Explicit setting of values for a particular energy point"
        self.xcent[ie]=xcent
        self.dxup[ie]=abs(xup-xcent)
        self.dxdn[ie]=abs(xcent-xdn)
        
    def xsec(self,ie):
        "Return xsec central value"
        return self.xcent[ie]

    def xsecup(self,ie):
        "Return xsec positive envenlope"
        return self.xcent[ie]+self.dxup[ie]

    def xsecdn(self,ie):
        "Return xsec negative envelope"
        return self.xcent[ie]-self.dxdn[ie]

    def fillPred(self,predpars):
        "Fill prediction arrays, given a0-6 parameters for up/central/down"
        for ie in range(self.npt):
            sqrts=self.xevals[ie]*1000.
            self.xcent[ie]=self.xsecPred(sqrts,predpars[1])
            self.dxup[ie]=self.xsecPred(sqrts,predpars[0])-self.xcent[ie]
            self.dxdn[ie]=self.xcent[ie]-self.xsecPred(sqrts,predpars[2])

    def xsecPred(self,ecm,p):
        "Return cross-section for given ECm (GeV) and parameterisation p[]"
        x=math.log(ecm/14000.)
        xx=x*x
        return p[0]+ecm*(p[1]+p[3]*x+p[4]*xx)+ecm*ecm*(p[2]+p[5]*x+p[6]*xx)

    def addError(self,epred):
        "Add the uncertanties from epred to this parameterisation"
        print("Adding errors from %s to %s" % (epred.name,self.name))
        if epred.npt!=self.npt:
            raise Exception("EPred.addError sizes do not match %i %i" % (epred.npt,self.npt))
        for ie in range(self.npt):
            self.dxup[ie]=math.sqrt(self.dxup[ie]**2+epred.dxup[ie]**2)
            self.dxdn[ie]=math.sqrt(self.dxdn[ie]**2+epred.dxdn[ie]**2)

    def printXSec(self,ofile=None):
        "Printout the cross-section vs energy,optionally also to file"
        for ie in range(self.npt):
            print("Cross-section scan for %s with %i points" % (self.name,self.npt))
            print("%3i %5.2f %7.2f +%6.2f -%6.2f" % (ie,self.xevals[ie],self.xcent[ie],self.dxup[ie],self.dxdn[ie]))
            if ofile:
                ofile.write("%3i %5.2f %7.2f +%6.2f -%6.2f\n" % (ie,self.xevals[ie],self.xcent[ie],self.dxup[ie],self.dxdn[ie]))

    def makeTGA(self):
        "Create TGraphAsymErrors to represent this prediction and error"
        ee=array('f',self.npt*[0.])
        tga=TGraphAsymmErrors(self.npt,self.xevals,self.xcent,ee,ee,self.dxdn,self.dxup)
        tga.SetNameTitle(self.name,self.name)
        return tga

    def makeRatioRef(self,refpdf):
        "Make a TGraphAsymmErrors to represent ratio to reference PDF"
        rval=array('f',self.npt*[0.])
        rup=array('f',self.npt*[0.])
        rdn=array('f',self.npt*[0.])
        ee=array('f',self.npt*[0.])
        for ie in range(self.npt):
            rval[ie]=self.xcent[ie]/refpdf.xcent[ie]
            rup[ie]=self.dxup[ie]/self.xcent[ie]*rval[ie]
            rdn[ie]=self.dxdn[ie]/self.xcent[ie]*rval[ie]
        tga=TGraphAsymmErrors(self.npt,self.xevals,rval,ee,ee,rdn,rup)
        return tga

    def makeSigmaBands(self,refpdf):
        "Make TGraphs for up and down 1 sigma bands on ratio"
        rup=array('f',self.npt*[0.])
        rdn=array('f',self.npt*[0.])
        for ie in range(self.npt):
            rval=self.xcent[ie]/refpdf.xcent[ie]
            rup[ie]=(1.+self.dxup[ie]/self.xcent[ie])*rval
            rdn[ie]=(1.-self.dxdn[ie]/self.xcent[ie])*rval
        tgup=TGraph(self.npt,self.xevals,rup)
        tgdn=TGraph(self.npt,self.xevals,rdn)
        return (tgup,tgdn)

# actually produce the plot in xsec_ecm.pdf
a=ECMPlot()
a.plot()
a.p.dumpPlot('Sigma_ttbar',wait=False)
