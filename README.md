# ttXsRun3_trex [![build status](https://gitlab.cern.ch/pinamont/ttxsrun3_trex/badges/master/pipeline.svg "build status")](https://gitlab.cern.ch/pinamont/ttxsrun3_trex/commits/master)

## Setup

   * clone this repo inside your TRExFitter directory

## Run 5 TeV test

   * create a direcrory `DilepInputs/` inside your main TRExFitter direcrory:
```bash
mkdir DilepInputs
```
   * from your main TRExFitter directory, run the ROOT macro `CreateDilepXS_histos.C`, e.g.:
```bash
root -l -b -q ttXsRun3_trex/CreateDilepXS_histos.C
```
   * run TRExFitter to read these inputs:
```bash
trex-fitter h ttXsRun3_trex/dilep_xsec_5TeVtest.config
```
   * run the fit:
```bash
trex-fitter wf ttXsRun3_trex/dilep_xsec_5TeVtest.config
```
